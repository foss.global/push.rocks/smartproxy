import * as plugins from './smartproxy.plugins.js';

export class SslRedirect {
  httpServer: plugins.http.Server;
  port: number;
  constructor(portArg: number) {
    this.port = portArg;
  }

  public async start() {
    this.httpServer = plugins.http.createServer((request, response) => {
      const requestUrl = new URL(request.url, `http://${request.headers.host}`);
      const completeUrlWithoutProtocol = `${requestUrl.host}${requestUrl.pathname}${requestUrl.search}`;
      const redirectUrl = `https://${completeUrlWithoutProtocol}`;
      console.log(`Got http request for http://${completeUrlWithoutProtocol}`);
      console.log(`Redirecting to ${redirectUrl}`);
      response.writeHead(302, {
        Location: redirectUrl,
      });
      response.end();
    });
    this.httpServer.listen(this.port);
  }

  public async stop() {
    const done = plugins.smartpromise.defer();
    this.httpServer.close(() => {
      done.resolve();
    });
    await done.promise;
  }
}