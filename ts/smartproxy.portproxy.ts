import * as plugins from './smartproxy.plugins.js';
import * as net from 'net';

export class PortProxy {
  netServer: plugins.net.Server;
  fromPort: number;
  toPort: number;

  constructor(fromPortArg: number, toPortArg: number) {
    this.fromPort = fromPortArg;
    this.toPort = toPortArg;
  }

  public async start() {
    const cleanUpSockets = (from: plugins.net.Socket, to: plugins.net.Socket) => {
      from.end();
      to.end();
      from.removeAllListeners();
      to.removeAllListeners();
      from.unpipe();
      to.unpipe();
      from.destroy();
      to.destroy();
    };
    this.netServer = net
      .createServer((from) => {
        const to = net.createConnection({
          host: 'localhost',
          port: this.toPort,
        });
        from.setTimeout(120000);
        from.pipe(to);
        to.pipe(from);
        from.on('error', () => {
          cleanUpSockets(from, to);
        });
        to.on('error', () => {
          cleanUpSockets(from, to);
        });
        from.on('close', () => {
          cleanUpSockets(from, to);
        });
        to.on('close', () => {
          cleanUpSockets(from, to);
        });
        from.on('timeout', () => {
          cleanUpSockets(from, to);
        });
        to.on('timeout', () => {
          cleanUpSockets(from, to);
        });
        from.on('end', () => {
          cleanUpSockets(from, to);
        });
        to.on('end', () => {
          cleanUpSockets(from, to);
        });
      })
      .listen(this.fromPort);
    console.log(`PortProxy -> OK: Now listening on port ${this.fromPort}`);
  }

  public async stop() {
    const done = plugins.smartpromise.defer();
    this.netServer.close(() => {
      done.resolve();
    });
    await done.promise;
  }
}
