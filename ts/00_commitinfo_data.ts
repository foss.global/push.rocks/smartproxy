/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartproxy',
  version: '3.0.58',
  description: 'a proxy for handling high workloads of proxying'
}
